<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/facultades', 'FacultadesController@index');
Route::get('/pedidos', 'PedidosController@index');
Route::get('/pedidos/detalle/{id}', 'PedidosController@indexDetalle')->name('detallepedido');

Route::get('/pedidos/facultad/{codfacultad}', 'PedidosController@indexFacultades');

Route::get('/pedidos/facultadsinu/edit/{pedido_id}/{codfacultad}/{codmodalidad}/{niv_formacion}', 'FacultadesController@editFacultadSinu');
Route::put('/pedidos/facultadsinu/update', 'FacultadesController@updateFacultadSinu');

Route::get('/pedidos/programasinu/edit/{pedido_id}/{codfacultad}/{codprograma}', 'ProgramasController@editProgramaSinu');
Route::put('/pedidos/programasinu/update', 'ProgramasController@updateProgramaSinu');

Route::get('/pedidosresumen/datatable', 'PedidosController@datatablePedidosResumen')->name('datatable.pedidosresumen');
Route::get('/facultadessinuresumen/datatable/{codfacultad}', 'FacultadesController@datatableFacultadesSinuResumen')->name('datatable.facultadessinuresumen');
Route::get('/estudiantespedido/datatable/{codfacultad}/{codmodalidad}', 'EstudiantesController@datatableEstudiantesPedido')->name('datatable.estudiantespedido');
Route::get('/facultadessinu/datatable/{pedido_id}/{codfacultad}', 'FacultadesController@datatableFacultadesSinu')->name('datatable.facultadessinu');
Route::get('/programassinu/datatable/{codfacultad}', 'ProgramasController@datatableProgramasSinu')->name('datatable.programassinu');


Route::get('viewArchivosGenerados', 'PedidosController@viewArchivosGenerados');
Route::get('validarPedidos', 'PedidosController@validarPedidos');


//Route::get('/facultadessinu/datatable', 'FacultadesController@datatableFacultadesSinu')->name('datatable.facultadessinu');
//Route::get('/programassinu/datatable', 'FacultadesController@datatableProgramasSinu')->name('datatable.programassinu');

//Route::get('/estudiantessinu/datatable', 'FacultadesController@datatableEstudiantesSinu')->name('datatable.estudiantessinu');

Route::get('/', 'ProgramasController@index');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
