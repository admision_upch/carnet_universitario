<div class="portlet-body">
    <div class="tabbable-custom ">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_5_1" data-toggle="tab"><i class="icon-refresh font-dark" id="bt_refresh_facultadessinuresumen" ></i> Resumen Facultades </a>
            </li>
            <li>
                <a href="#tab_5_2" data-toggle="tab"> Detalle Estudiantes </a>
            </li>
            <li>
                <a href="#tab_5_3" data-toggle="tab"> Match SINU - SUNEDU </a>
            </li>
            <li>
                <a href="#tab_5_4" data-toggle="tab"> Exportar Archivo SUNEDU </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_5_1">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="facultadessinuresumen">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Pedido</th>
                                <th>Facultad</th>
                                <th>Total Estudiantes</th>
                                <th>Pedidos Confirmados</th>
                                <th>Pedidos Sin Confirmar</th>
                                <th>Pedidos Confirmados - Pregrado</th>
                                <th>Pedidos Confirmados - Postgrado</th>
                                <th>Pedidos Solicitados</th>
                                <th>Confirmacion Pregrado</th>
                                <th>Confirmacion Postgrado</th>
                            </tr>
                        </thead>

                    </table>





                </div>
            </div>
            <div class="tab-pane" id="tab_5_2">
                <div class="portlet-body">
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a href="#tab_2_1" data-toggle="tab"> Pregrado </a>
                        </li>
                        <li>
                            <a href="#tab_2_2" data-toggle="tab"> Postgrado </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_2_1">


                            <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantespregradopedido">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Pedido</th>
                                        <th>Cod.Estudiante</th>
                                        <th>Tipo Documento</th>
                                        <th>N° Documento</th>
                                        <th>Nombre</th>
                                        <th>Apellido Paterno</th>
                                        <th>Apellido Materno</th>
                                        <th>Facultad Sinu</th>
                                        <th>Facultad Sunedu</th>
                                        <th>Programa Sinu</th>
                                        <th>Programa Sunedu</th>
                                        <th>Año Ingreso</th>
                                        <th>Periodo Ingreso</th>
                                        <th>Estado</th>
                                        <th>Modalidad</th>
                                        <th>foto</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_2_2">


                            <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="estudiantespostgradopedido">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Pedido</th>
                                        <th>Cod.Estudiante</th>
                                        <th>Tipo Documento</th>
                                        <th>N° Documento</th>
                                        <th>Nombre</th>
                                        <th>Apellido Paterno</th>
                                        <th>Apellido Materno</th>
                                        <th>Facultad Sinu</th>
                                        <th>Facultad Sunedu</th>
                                        <th>Programa Sinu</th>
                                        <th>Programa Sunedu</th>
                                        <th>Año Ingreso</th>
                                        <th>Periodo Ingreso</th>
                                        <th>Estado</th>
                                        <th>Modalidad</th>
                                        <th>foto</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>

                    </div>












                </div>
            </div>
            <div class="tab-pane" id="tab_5_3">
                <div class="portlet-body">
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a href="#tab_3_1" data-toggle="tab"> Facultades SINU - SUNEDU </a>
                        </li>
                        <li>
                            <a href="#tab_3_2" data-toggle="tab"> Programas SINU - SUNEDU </a>
                        </li>

                    </ul>


                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_3_1">


                            <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="facultadessinu">
                                <thead>
                                    <tr>

                                        <th>Pedido</th>
                                        <th>Cod. Facultad Sinu</th>
                                        <th>Abrv.Facultad Sinu</th>
                                        <th>Facultad Sinu</th>
                                        <th>Modalidad</th>
                                        <th>Formacion</th>
                                        <th>Cod. Facultad Sunedu</th>
                                        <th>Facultad Sunedu</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab_3_2">

                            <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="programassinu">
                                <thead>
                                    <tr>
                                        <th>Pedido</th>
                                        <th>Cod Facultad Sinu</th>
                                        <th>Facultad Sinu</th>
                                        <th>Modalidad</th>
                                        <th>Formacion</th>
                                        <th>Codigo<br> Programa Sinu</th>
                                        <th>Programa Sinu</th>
                                        <th>Codigo <br>Programa Sunedu</th>
                                        <th>Programa Sunedu</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_5_4">




                <div class="row">
                    <div class="col-lg-6">
                        <div class="portlet light portlet-fit ">

                            <div class="portlet-body">

                                <button id="validarPedido" type="button" data-loading-text="Cargando..." class="btn btn-primary ladda-button btn-block" data-style="expand-left">
                                    <i class="glyphicon glyphicon-cog"> </i>   <span class="ladda-label">Procesar Pedido</span>
                                </button>


                                <div class="mt-element-list">

                                </div>
                            </div>
                        </div>


                    </div>
                </div>




            </div>
        </div>
    </div>

</div>













<script type="text/javascript">
    /* Formatting function for row details - modify as you need */



    function formatoTable2(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Total Estudiantes:</td>' +
                '<td>' + d.total_estudiantes + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Confirmados:</td>' +
                '<td>' + d.total_pedidos_confirmados + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Sin Confirmar:</td>' +
                '<td>' + d.total_pedidos_sinconfirmar + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Solicitados:</td>' +
                '<td>' + d.total_pedidos_solicitados + '</td>' +
                '</tr>' +
                '</table>';
    }
    function formatoTable3(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Total Estudiantes:</td>' +
                '<td>' + d.total_estudiantes + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Confirmados:</td>' +
                '<td>' + d.total_pedidos_confirmados + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Sin Confirmar:</td>' +
                '<td>' + d.total_pedidos_sinconfirmar + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Solicitados:</td>' +
                '<td>' + d.total_pedidos_solicitados + '</td>' +
                '</tr>' +
                '</table>';
    }
    function formatoTable4(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Total Estudiantes:</td>' +
                '<td>' + d.total_estudiantes + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Confirmados:</td>' +
                '<td>' + d.total_pedidos_confirmados + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Sin Confirmar:</td>' +
                '<td>' + d.total_pedidos_sinconfirmar + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Solicitados:</td>' +
                '<td>' + d.total_pedidos_solicitados + '</td>' +
                '</tr>' +
                '</table>';
    }
    function formatoTable5(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Total Estudiantes:</td>' +
                '<td>' + d.total_estudiantes + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Confirmados:</td>' +
                '<td>' + d.total_pedidos_confirmados + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Sin Confirmar:</td>' +
                '<td>' + d.total_pedidos_sinconfirmar + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Solicitados:</td>' +
                '<td>' + d.total_pedidos_solicitados + '</td>' +
                '</tr>' +
                '</table>';
    }
    function formatoTable6(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Total Estudiantes:</td>' +
                '<td>' + d.total_estudiantes + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Confirmados:</td>' +
                '<td>' + d.total_pedidos_confirmados + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Sin Confirmar:</td>' +
                '<td>' + d.total_pedidos_sinconfirmar + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Solicitados:</td>' +
                '<td>' + d.total_pedidos_solicitados + '</td>' +
                '</tr>' +
                '</table>';
    }

    function viewArchivosGenerados() {
        $.ajax({
            type: "GET",
            url: '/viewArchivosGenerados',
            //data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
            success: function (data)
            {
                $(".mt-element-list").html(data);

            }
        });
    }

    $(document).ready(function () {

        $("#validarPedido").click(function () {
            $(".mt-element-list").html("");
            var t = Ladda.create(document.querySelector('#validarPedido'));
            t.start();
            $.ajax({
                type: "GET",
                url: '/validarPedidos',
                //data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    viewArchivosGenerados();
                    swal(data.message, null, data.status);
                    t.stop();

                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });




        $('#bt_refresh_facultadessinuresumen').click(function () {
            oTable2.ajax.reload();
        });

        oTable2 = $('#facultadessinuresumen').DataTable({

            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.facultadessinuresumen', ['codfacultad' => $codfacultad]) }}",
            "columns": [
                {
                    "className": 'control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "searchable": false
                },
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu', "width": "100px"},
                {data: 'total_estudiantes', name: 'total_estudiantes', "width": "10%"},
                {data: 'total_pedidos_confirmados', name: 'total_pedidos_confirmados', "width": "10%"},
                {data: 'total_pedidos_sinconfirmar', name: 'total_pedidos_sinconfirmar', "width": "10%"},
                {data: 'total_pedidos_confirmados_pregrado', name: 'total_pedidos_confirmados_pregrado', "width": "10%", visible: false},
                {data: 'total_pedidos_confirmados_postgrado', name: 'total_pedidos_confirmados_postgrado', "width": "10%", visible: false},
                {data: 'total_pedidos_solicitados', name: 'total_pedidos_solicitados', "width": "10%", visible: false},
                {data: 'confirmacion_pregrado', name: 'confirmacion_pregrado', "width": "10%"},
                {data: 'confirmacion_postgrado', name: 'confirmacion_postgrado', "width": "10%"}

            ]

        });


        $('#facultadessinuresumen tbody').on('click', 'td.control', function () {
            var tr = $(this).closest('tr');
            var row = oTable2.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatoTable2(row.data())).show();
                tr.addClass('shown');
            }
        });



        oTable3 = $('#estudiantespregradopedido').DataTable({
            oLanguage: {
                sProcessing: "procesando ...."
            },
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.estudiantespedido', ['codfacultad' => $codfacultad, 'codmodalidad' => '1']) }}",
            "order": [[7, "asc"]],
            "columns": [
                {
                    "className": 'control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "searchable": false
                },
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codalumno', name: 'codalumno'},
                {data: 'tipodocumento', name: 'tipodocumento', visible: false},
                {data: 'nrodocumento', name: 'nrodocumento'},
                {data: 'nombre', name: 'nombre'},
                {data: 'apepaterno', name: 'apepaterno'},
                {data: 'apematerno', name: 'apematerno'},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu'},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu', visible: false},
                {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu', visible: false},
                {data: 'ano_ing', name: 'ano_ing', visible: false},
                {data: 'per_ing', name: 'per_ing', visible: false},
                {data: 'estado', name: 'estado'

                    , "render": function (data, type, row) {

                        retorno = "<select>";
                        if (row['estado'] == '1') {

                            retorno += '<option value="1" selected >Confirmado</option>';
                            retorno += '<option value="0">Sin Confirmar</option>';
                        } else if (row['estado'] == '0') {

                            retorno += '<option value="1" selected >Confirmado</option>';
                            retorno += '<option value="0" selected >Sin Confirmar</option>';
                        }
                        retorno += "</select>";
                        return retorno;
                    }
                },
                {data: 'codmodalidad', name: 'codmodalidad', visible: false},
                {data: 'foto', name: 'foto'}


            ]

        });
        $('#estudiantespregradopedido tbody').on('click', 'td.control', function () {
            var tr = $(this).closest('tr');
            var row = oTable3.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatoTable3(row.data())).show();
                tr.addClass('shown');
            }
        });



        oTable4 = $('#estudiantespostgradopedido').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.estudiantespedido', ['codfacultad' => $codfacultad, 'codmodalidad' => '2']) }}",
            "order": [[7, "asc"]],
            "columns": [
                {
                    "className": 'control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "searchable": false
                },
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codalumno', name: 'codalumno'},
                {data: 'tipodocumento', name: 'tipodocumento', visible: false},
                {data: 'nrodocumento', name: 'nrodocumento'},
                {data: 'nombre', name: 'nombre'},
                {data: 'apepaterno', name: 'apepaterno'},
                {data: 'apematerno', name: 'apematerno'},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu'},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu', visible: false},
                {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu', visible: false},
                {data: 'ano_ing', name: 'ano_ing', visible: false},
                {data: 'per_ing', name: 'per_ing', visible: false},
                {data: 'estado', name: 'estado'
                    , "render": function (data, type, row) {

                        retorno = "<select>";
                        if (row['estado'] == '1') {

                            retorno += '<option value="1" selected >Confirmado</option>';
                            retorno += '<option value="0">Sin Confirmar</option>';
                        } else if (row['estado'] == '0') {

                            retorno += '<option value="1" selected >Confirmado</option>';
                            retorno += '<option value="0" selected >Sin Confirmar</option>';
                        }
                        retorno += "</select>";
                        return retorno;
                    }

                },
                {data: 'codmodalidad', name: 'codmodalidad', visible: false},
                {data: 'foto', name: 'foto'}


            ]

        });
        $('#estudiantespostgradopedido tbody').on('click', 'td.control', function () {
            var tr = $(this).closest('tr');
            var row = oTable4.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatoTable4(row.data())).show();
                tr.addClass('shown');
            }
        });


        oTable5 = $('#facultadessinu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.facultadessinu', ['pedido_id' => $pedido_id,'codfacultad' => $codfacultad]) }}",
            "order": [[1, "desc"]],
            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codfacultad', name: 'codfacultad', visible: true},
                {data: 'abreviaturafacultad', name: 'abreviaturafacultad'},
                {data: 'nomfacultad', name: 'nomfacultad'},
                {data: 'modalidad_desc', name: 'modalidad_desc'},
                {data: 'formacion', name: 'formacion'},
                {data: 'codfacultad_sunedu', name: 'codfacultad_sunedu'},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });


        $('#facultadessinu tbody').on('click', 'td.control', function () {
            var tr = $(this).closest('tr');
            var row = oTable5.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatoTable5(row.data())).show();
                tr.addClass('shown');
            }
        });



        oTable6 = $('#programassinu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.programassinu', ['codfacultad' => $codfacultad]) }}",

            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codfacultad', name: 'codfacultad', visible: false},
                {data: 'nomfacultad', name: 'nomfacultad', visible: true},
                {data: 'modalidad_desc', name: 'modalidad_desc', visible: true},
                {data: 'formacion', name: 'formacion', visible: true},
                {data: 'codprograma', name: 'codprograma'},
                {data: 'nomprograma', name: 'nomprograma'},
                {data: 'codprograma_sunedu', name: 'codprograma_sunedu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "order": [[2, "asc"]],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(2, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {


                        $(rows).eq(i).before(
                                '<tr class="group color md-bg-blue-500" style="font-weight:bold;"  ><td colspan="8">' + group + '</td></tr>'
                                );

                        last = group;
                    }
                });
            }
        });




        $('#programassinu tbody').on('click', 'tr.group', function () {
            var currentOrder = oTable6.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                oTable6.order([1, 'desc']).draw();
            } else {
                oTable6.order([1, 'asc']).draw();
            }
        });




    });








</script>  
