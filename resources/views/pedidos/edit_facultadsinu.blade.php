@extends ('layouts.modal_formulario')    
@section('content')   


<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase">Facultad SINU - Editar</span>
        </div>
    </div>
    <div class="portlet-body form">
        <div id="respuesta"  ></div>
        <form role="form"  method="POST" id="formulario" name="formulario" >
            {{ csrf_field() }}
            
            <input type="hidden" class="form-control" id="pedido_id" name="pedido_id" value="{{ $facultadSinu->pedido_id}}" readonly="">
            <input type="hidden" class="form-control" id="codmodalidad" name="codmodalidad" value="{{ $facultadSinu->codmodalidad}}" readonly="">
            <input type="hidden" class="form-control" id="niv_formacion" name="niv_formacion" value="{{ $facultadSinu->niv_formacion}}" readonly="">
            <div class="form-body">
                <div class="form-group">
                    <label>Cod.Facultad</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="codfacultad" name="codfacultad" value="{{ $facultadSinu->codfacultad}}" readonly="">
                    </div>
                </div>

                <div class="form-group">
                    <label>Facultad</label>

                    <input type="text" class="form-control" placeholder="{{ $facultadSinu->nomfacultad}}" readonly="">

                </div>

                {!! $selectFacultadSunedu !!}

            </div>
            <div class="form-actions right">
                <button type="button" class="btn default">Cancelar</button>
                <button type="submit" class="btn blue" id="btn_enviar">Guardar</button>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->




@endsection


@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        $("#btn_enviar").click(function () {
            var url = "/pedidos/facultadsinu/update"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "PUT",
                url: url,
                data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    $('#ajax_modal').modal('toggle');
                    swal(data.message, null, data.status);
                    $("#respuesta").html(data.status); // Mostrar la respuestas del script PHP.
                    parent.oTable5.ajax.reload();
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


</script>




@endsection