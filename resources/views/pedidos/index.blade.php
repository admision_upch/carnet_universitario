@extends ('layouts.index')         
@section('css')   


@endsection
@section('title')   
<h1>Pedidos de Carnés Universitarios
    <!--small>responsive extension demos</small-->
</h1>
@endsection
@section('content')   

<div class="m-heading-1 border-green m-bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="icon-refresh font-dark" id="bt_refresh_pedidosresumen" ></i>

            <span class="caption-subject bold uppercase">Pedidos </span>
        </div>
        <div class="tools"> </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover  dtr-column collapsed" width="100%" id="pedidosresumen">
            <thead>
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>Fecha Solicitud</th>
                    <th>Total Estudiantes</th>
                    <th>Pedidos Confirmados</th>
                    <th>Pedidos Sin Confirmar</th>
                    <th>Pedidos Solicitados</th>

                    <th>Acción</th>
                </tr>
            </thead>

        </table>
    </div>
</div>

<div class="m-heading-1 border-green m-bordered" id="detallePedido">


</div>


@endsection


@section('js')


<script type="text/javascript">
    function formatoTable(d) {
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Total Estudiantes:</td>' +
                '<td>' + d.total_estudiantes + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Confirmados:</td>' +
                '<td>' + d.total_pedidos_confirmados + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Sin Confirmar:</td>' +
                '<td>' + d.total_pedidos_sinconfirmar + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Pedidos Solicitados:</td>' +
                '<td>' + d.total_pedidos_solicitados + '</td>' +
                '</tr>' +
                '</table>';
    }

    $(document).ready(function () {

        $('#bt_refresh_pedidosresumen').click(function () {
            oTable.ajax.reload();
        });
        oTable = $('#pedidosresumen').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.pedidosresumen') }}",
            "columns": [
                {
                    "className": 'control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "searchable": false
                },
                {data: 'id', name: 'id'},
                {data: 'nombre', name: 'nombre'},
                {data: 'fec_inicio', name: 'fec_inicio'},
                {data: 'fec_fin', name: 'fec_fin'},
                {data: 'fec_solicitud', name: 'fec_solicitud'},
                {data: 'total_estudiantes', name: 'total_estudiantes', visible: false},
                {data: 'total_pedidos_confirmados', name: 'total_pedidos_confirmados', visible: false},
                {data: 'total_pedidos_sinconfirmar', name: 'total_pedidos_sinconfirmar', visible: false},
                {data: 'total_pedidos_solicitados', name: 'total_pedidos_solicitados', visible: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        $('#pedidosresumen tbody').on('click', 'td.control', function () {
            var tr = $(this).closest('tr');
            var row = oTable.row(tr);
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(formatoTable(row.data())).show();
                tr.addClass('shown');
            }
        });
    });
    function verDetallePedido(pedido_id) {
       $("#detallePedido").html("cargando...");
       $.ajax({
            type: "GET",
            url: "/pedidos/detalle/" + pedido_id,
            success: function (data) {
                //console.log(data);
                $('#detallePedido').html(data);
            }
        })
    }



</script>  

@endsection



