@extends ('layouts.master')         


@section('content')



<div id="content_wrapper" class="simple">
    <div id="header_wrapper" class="header-sm">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <header id="header">
                        <h1>Pedidos de Carnés  </h1>
                        <ol class="breadcrumb">
                            <li><a href="index.html">Dashboard</a></li>

                        </ol>
                    </header>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="content">
                    <div class="content-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="card" id="sweet_alerts_card">
                                    <header class="card-heading ">
                                        <h2 class="card-title">Pedidos de Carnés </h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="pedidosresumen" class="table table-hover table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Nombre</th>
                                                        <th>Total Estudiantes</th>
                                                        <th>Pedidos Confirmados</th>
                                                        <th>Pedidos Sin Confirmar</th>
                                                        <th>Pedidos Solicitados</th>
                                                        <th>Fecha Inicio</th>
                                                        <th>Fecha Fin</th>
                                                        <th>Fecha Solicitud</th>

                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12">


                                <div class="card">

                                    <div class="card-body">
                                        <ul class="nav nav-pills nav-pills-primary" role="tablist">
                                            <li class="active">
                                                <a href="#pillnav1" role="tab" data-toggle="tab">
                                                    <i class="zmdi zmdi-home"></i> Resumen Facultades
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#pillnav2" role="tab" data-toggle="tab">
                                                    <i class="zmdi zmdi-calendar-note"></i> Detalle Estudiantes
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#pillnav3" role="tab" data-toggle="tab">
                                                    <i class="zmdi zmdi-calendar-note"></i> Match Facultades SINU - SUNEDU
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#pillnav4" role="tab" data-toggle="tab">
                                                    <i class="zmdi zmdi-calendar-note"></i> Match Programas SINU - SUNEDU
                                                </a>
                                            </li>

                                        </ul>
                                        <div id="pillContent1" class="tab-content m-t-15">
                                            <div class="tab-pane fade active in" id="pillnav1">


                                                <div class="card" id="sweet_alerts_card">
                                                    <header class="card-heading ">
                                                        <h2 class="card-title">Facultades</h2>
                                                    </header>
                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                            <table id="facultadessinuresumen" class="table table-hover table-condensed">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Pedido</th>
                                                                        <th>Facultad</th>
                                                                        <th>Total Estudiantes</th>
                                                                        <th>Pedidos Confirmados</th>
                                                                        <th>Pedidos Sin Confirmar</th>
                                                                        <th>Pedidos Confirmados - Pregrado</th>
                                                                        <th>Pedidos Confirmados - Postgrado</th>
                                                                        <th>Pedidos Solicitados</th>
                                                                        <th>Confirmacion Pregrado</th>
                                                                        <th>Confirmacion Postgrado</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>








                                            </div>
                                            <div class="tab-pane fade" id="pillnav2">

                                                <div class="card" id="sweet_alerts_card">
                                                    <header class="card-heading ">
                                                        <h2 class="card-title">Estudiantes</h2>
                                                    </header>
                                                    <div class="card-body">

                      <div class="card-body p-t-0">
                                                            <div class="card card-transparent m-b-0">
                                                                <div class="card-body p-0">
                                                                    <div class="tabpanel">
                                                                        <ul class="nav nav-tabs">
                                                                            <li class="active" role="presentation"><a href="#tab-5" data-toggle="tab" aria-expanded="true">Pregrado</a></li>
                                                                            <li role="presentation"><a href="#tab-6" data-toggle="tab" aria-expanded="true">Postgrado</a></li>

                                                                        </ul>
                                                                    </div>
                                                                    <div class="tab-content  p-20">
                                                                        <div class="tab-pane fadeIn active" id="tab-5">
                                                                            <div class="table-responsive">
                                                                                <table id="estudiantespregradopedido" class="table table-hover table-condensed ">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Pedido</th>
                                                                                            <th>Cod.Estudiante</th>
                                                                                            <th>Tipo Documento</th>
                                                                                            <th>N° Documento</th>
                                                                                            <th>Nombre</th>
                                                                                            <th>Apellido Paterno</th>
                                                                                            <th>Apellido Materno</th>
                                                                                            <th>Facultad Sinu</th>
                                                                                            <th>Facultad Sunedu</th>
                                                                                            <th>Programa Sinu</th>
                                                                                            <th>Programa Sunedu</th>
                                                                                            <th>Año Ingreso</th>
                                                                                            <th>Periodo Ingreso</th>
                                                                                            <th>Estado</th>
                                                                                            <th>Modalidad</th>
                                                                                            <th>foto</th>


                                                                                        </tr>
                                                                                    </thead>
                                                                                </table>

                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fadeIn" id="tab-6">
                                                                            <div class="table-responsive">
                                                                                <table id="estudiantespostgradopedido" class="table table-hover table-condensed ">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Pedido</th>
                                                                                            <th>Cod.Estudiante</th>
                                                                                            <th>Tipo Documento</th>
                                                                                            <th>N° Documento</th>
                                                                                            <th>Nombre</th>
                                                                                            <th>Apellido Paterno</th>
                                                                                            <th>Apellido Materno</th>
                                                                                            <th>Facultad Sinu</th>
                                                                                            <th>Facultad Sunedu</th>
                                                                                            <th>Programa Sinu</th>
                                                                                            <th>Programa Sunedu</th>
                                                                                            <th>Año Ingreso</th>
                                                                                            <th>Periodo Ingreso</th>
                                                                                            <th>Estado</th>
                                                                                            <th>Modalidad</th>
                                                                                            <th>foto</th>


                                                                                        </tr>
                                                                                    </thead>
                                                                                </table>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                          


                                                    </div>
                                                </div>


                                            </div>

                                            <div class="tab-pane fade" id="pillnav3">

                                                <div class="card" id="sweet_alerts_card">
                                                    <header class="card-heading ">
                                                        <h2 class="card-title"> Match Facultades SINU - SUNEDU</h2>
                                                    </header>
                                                    <div class="card-body">


                                                        <div class="table-responsive">
                                                            <table id="relacionfacultadessinusunedu" class="table table-hover table-condensed">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Pedido</th>
                                                                        <th>Cod. Facultad Sinu</th>
                                                                        <th>Facultad Sinu</th>
                                                                        <th>Cod. Facultad Sunedu</th>
                                                                        <th>Facultad Sunedu</th>
                                                                        <th>Acción</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>

                                                        </div>


                                                    </div>
                                                </div>


                                            </div>



                                            <div class="tab-pane fade" id="pillnav4">

                                                <div class="card" id="sweet_alerts_card">
                                                    <header class="card-heading ">
                                                        <h2 class="card-title"> Match Programas SINU - SUNEDU</h2>
                                                    </header>
                                                    <div class="card-body">


                                                        <div class="table-responsive">
                                                            <table id="relacionprogramassinusunedu" class="table table-hover table-condensed">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Pedido</th>
                                                                        <th>Cod Facultad Sinu</th>
                                                                        <th>Facultad Sinu</th>
                                                                        <th>Cod. Programa Sinu</th>
                                                                        <th>Programa Sinu</th>
                                                                        <th>Cod. Programa Sunedu</th>
                                                                        <th>Programa Sunedu</th>
                                                                        <th>Acción</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>

                                                        </div>


                                                    </div>
                                                </div>


                                            </div>



                                        </div>
                                    </div>
                                </div>



                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="chat_compose_wrapper">
        <div class="tippy-top">
            <div class="recipient">Allison Grayce</div>
            <ul class="card-actions icons  right-top">
                <li>
                    <a href="javascript:void(0)">
                        <i class="zmdi zmdi-videocam"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu btn-primary dropdown-menu-right">
                        <li>
                            <a href="javascript:void(0)">Option One</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Option Two</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Option Three</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" data-chat="close">
                        <i class="zmdi zmdi-close"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class='chat-wrapper scrollbar'>
            <div class='chat-message scrollbar'>
                <div class='chat-message chat-message-recipient'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/05.jpg' />
                    <div class='chat-message-wrapper'>
                        <div class='chat-message-content'>
                            <p>Hey Mike, we have funding for our new project!</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
                <div class='chat-message chat-message-sender'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/02.jpg' />
                    <div class='chat-message-wrapper '>
                        <div class='chat-message-content'>
                            <p>Awesome! Photo booth banh mi pitchfork kickstarter whatever, prism godard ethical 90's cray selvage.</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
                <div class='chat-message chat-message-recipient'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/05.jpg' />
                    <div class='chat-message-wrapper'>
                        <div class='chat-message-content'>
                            <p> Artisan glossier vaporware meditation paleo humblebrag forage small batch.</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
                <div class='chat-message chat-message-sender'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/02.jpg' />
                    <div class='chat-message-wrapper'>
                        <div class='chat-message-content'>
                            <p>Bushwick letterpress vegan craft beer dreamcatcher kickstarter.</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="compose-footer">
            <form class="form-horizontal compose-form">
                <ul class="card-actions icons left-bottom">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="zmdi zmdi-attachment-alt"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="zmdi zmdi-mood"></i>
                        </a>
                    </li>
                </ul>
                <div class="form-group m-10 p-l-75 is-empty">
                    <div class="input-group">
                        <label class="sr-only">Leave a comment...</label>
                        <input type="text" class="form-control form-rounded input-lightGray" placeholder="Leave a comment..">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-blue btn-fab  btn-fab-sm">
                                <i class="zmdi zmdi-mail-send"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
        </footer>
    </section>
</div>
<footer id="footer_wrapper">
    <div class="footer-content">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h6>Want to Work with Us?</h6>
                <p>Paleo flexitarian bushwick letterpress, ea migas yr adipisicing. Man bun tacos tumblr kombucha, yuccie banjo affogato dolore gentrify retro chartreuse. Anim austin tempor ethical, sapiente food truck fanny pack farm-to-table.
                    Culpa keytar esse tilde hoodie, art party nostrud messenger bag authentic helvetica kinfolk cred eu affogato forage.</p>
            </div>
            <div class="col-xs-12 col-sm-2">
                <h6>Company</h6>
                <ul>
                    <li><a href="javascript:void(0)">About Us </a></li>
                    <li><a href="javascript:void(0)">Careers</a></li>
                    <li><a href="javascript:void(0)">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h6>Email Newsletters</h6>
                <p>Sign up for new MaterialWrap content, updates, and offers.</p>
                <div class="form-group is-empty">
                    <div class="input-group">
                        <label class="control-label sr-only" for="footerEmail">Email Address</label>
                        <input type="email" class="form-control" id="footerEmail" placeholder="Enter your email address...">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-white btn-fab animate-fab btn-fab-sm">
                                <i class="zmdi zmdi-mail-send"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copy-wrapper">
            <div class="col-xs-8">
                <p class="copy">&copy; Copyright <time class="year"></time> MaterialWrap - All Rights Reserved</p>
            </div>
            <div class="col-xs-4">
                <ul class="social">
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

@endsection


@section('js')


<script type="text/javascript">
    $(document).ready(function () {
        oTable = $('#pedidosresumen').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.pedidosresumen') }}",
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'nombre', name: 'nombre'},
                {data: 'total_estudiantes', name: 'total_estudiantes'},
                {data: 'total_pedidos_confirmados', name: 'total_pedidos_confirmados'},
                {data: 'total_pedidos_sinconfirmar', name: 'total_pedidos_sinconfirmar'},
                {data: 'total_pedidos_solicitados', name: 'total_pedidos_solicitados'},
                {data: 'fec_inicio', name: 'fec_inicio'},
                {data: 'fec_fin', name: 'fec_fin'},
                {data: 'fec_solicitud', name: 'fec_solicitud'}

            ]
        });

        oTable2 = $('#facultadessinuresumen').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.facultadessinuresumen', ['codfacultad' => $codfacultad]) }}",
            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu'},
                {data: 'total_estudiantes', name: 'total_estudiantes'},
                {data: 'total_pedidos_confirmados', name: 'total_pedidos_confirmados'},
                {data: 'total_pedidos_sinconfirmar', name: 'total_pedidos_sinconfirmar'},
                {data: 'total_pedidos_confirmados_pregrado', name: 'total_pedidos_confirmados_pregrado'},
                {data: 'total_pedidos_confirmados_postgrado', name: 'total_pedidos_confirmados_postgrado'},
                {data: 'total_pedidos_solicitados', name: 'total_pedidos_solicitados'},
                {data: 'confirmacion_pregrado', name: 'confirmacion_pregrado'},
                {data: 'confirmacion_postgrado', name: 'confirmacion_postgrado'}

            ]

        });


        oTable3 = $('#estudiantespregradopedido').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.estudiantespedido', ['codfacultad' => $codfacultad, 'codmodalidad' => '1']) }}",
            "order": [[7, "asc"]],
            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codalumno', name: 'codalumno'},
                {data: 'tipodocumento', name: 'tipodocumento', visible: false},
                {data: 'nrodocumento', name: 'nrodocumento'},
                {data: 'nombre', name: 'nombre'},
                {data: 'apepaterno', name: 'apepaterno'},
                {data: 'apematerno', name: 'apematerno'},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu', visible: false},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu', visible: false},
                {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                {data: 'ano_ing', name: 'ano_ing'},
                {data: 'per_ing', name: 'per_ing'},
                {data: 'estado', name: 'estado'
                    
                    , "render": function ( data, type, row ) {
                    //return data +' ('+ row['ano_ing']+')';
                    if (row['estado']=='0'){
                        
                        retorno='<input type="checkbox"  class="toggle-info">';
                    }else
                    {
                        retorno='<input type="checkbox" checked="checked" class="toggle-info">';
                    }
                    return retorno;
                }
                
                },
                
                {data: 'codmodalidad', name: 'codmodalidad', visible: false},
                {data: 'foto', name: 'foto'}


            ]

        });



      oTable4 = $('#estudiantespostgradopedido').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,
            "ajax": "{{ route('datatable.estudiantespedido', ['codfacultad' => $codfacultad, 'codmodalidad' => '2']) }}",
            "order": [[7, "asc"]],
            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codalumno', name: 'codalumno'},
                {data: 'tipodocumento', name: 'tipodocumento', visible: false},
                {data: 'nrodocumento', name: 'nrodocumento'},
                {data: 'nombre', name: 'nombre'},
                {data: 'apepaterno', name: 'apepaterno'},
                {data: 'apematerno', name: 'apematerno'},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu', visible: false},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu', visible: false},
                {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                {data: 'ano_ing', name: 'ano_ing'},
                {data: 'per_ing', name: 'per_ing'},
                {data: 'estado', name: 'estado'
                    
                    , "render": function ( data, type, row ) {
                    //return data +' ('+ row['ano_ing']+')';
                    if (row['estado']=='0'){
                        
                        retorno='<input type="checkbox"  class="toggle-info">';
                    }else
                    {
                        retorno='<input type="checkbox" checked="checked" class="toggle-info">';
                    }
                    return retorno;
                }
                
                },
              
                {data: 'codmodalidad', name: 'codmodalidad', visible: false},
                {data: 'foto', name: 'foto'}


            ]

        });

        oTable5 = $('#relacionfacultadessinusunedu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.relacionfacultadessinusunedu', ['codfacultad' => $codfacultad]) }}",
            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codfacultad', name: 'codfacultad'},
                {data: 'nomfacultad', name: 'nomfacultad'},
                {data: 'codfacultad_sunedu', name: 'codfacultad_sunedu'},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });




        oTable6 = $('#relacionprogramassinusunedu').DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": "{{ route('datatable.relacionprogramassinusunedu', ['codfacultad' => $codfacultad]) }}",
            "columns": [
                {data: 'pedido_id', name: 'pedido_id', visible: false},
                {data: 'codfacultad', name: 'codfacultad', visible: false},
                {data: 'nomfacultad', name: 'nomfacultad', visible: false},
                {data: 'codprograma', name: 'codprograma'},
                {data: 'nomprograma', name: 'nomprograma'},
                {data: 'codprograma_sunedu', name: 'codprograma_sunedu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "order": [[2, "asc"]],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(2, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {


                        $(rows).eq(i).before(
                                '<tr class="group color md-bg-blue-500" style="color:white;font-weight:bold;"  ><td colspan="8">' + group + '</td></tr>'
                                );

                        last = group;
                    }
                });
            }
        });


        $('#relacionprogramassinusunedu tbody').on('click', 'tr.group', function () {
            var currentOrder = oTable6.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                oTable6.order([1, 'desc']).draw();
            } else {
                oTable6.order([1, 'asc']).draw();
            }
        });


    });


</script>  


@endsection



