<!DOCTYPE html>
<html>
<head>
    <title>Laravel DataTables</title>
    <!--link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/plugin/datatables/dataTables.bootstrap.css"-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    
    
   <script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
</head>
<body>
 
<div class="container">
    <table id="programas" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th>Cod.Facultad - Sinu</th>
            <th>Facultad - Sinu</th>
            <th>Cod.Facultad - Sunedu</th>
            <th>Facultad - Sunedu</th>
            <th>Cod.Programa - Sinu</th>
            <th>Programa - Sinu</th>
            <th>Cod.Programa - Sunedu</th>
            <th>Programa - Sunedu</th>
            <th>Editar</th>
        </tr>
        </thead>
    </table>
</div>
 
<script type="text/javascript">
    $(document).ready(function() {
        oTable = $('#programas').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('datatable.programas') }}",
            "columns": [
                {data: 'codfacultad_sinu', name: 'codfacultad_sinu'},
                {data: 'nomfacultad_sinu', name: 'nomfacultad_sinu'},
                {data: 'codfacultad_sunedu', name: 'codfacultad_sunedu'},
                {data: 'nomfacultad_sunedu', name: 'nomfacultad_sunedu'},
                {data: 'codprograma_sinu', name: 'codprograma_sinu'},
                {data: 'nomprograma_sinu', name: 'nomprograma_sinu'},
                {data: 'codprograma_sunedu', name: 'codprograma_sunedu'},
                {data: 'nomprograma_sunedu', name: 'nomprograma_sunedu'},
                {
                "class": "center",
                "data": "done",
                "render": function (val, type, row) {
                    return val == 0 ? "To do" : '<input type="button" value="editar" > ' ;
                        }
                    }
            ]
        });
    });
</script>
</body>
</html>