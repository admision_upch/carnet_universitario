<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaSunedu extends Model
{
     protected $connection = 'pgsql_syscarnes';
     protected $table = 'programas_sunedu';
     protected $primaryKey = 'codprograma';
}
