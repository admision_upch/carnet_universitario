<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ProgramaSinu;
use App\ProgramaSunedu;
use App\FacultadSinu;
use App\FacultadSunedu;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\componentes\FormulariosController;

class ProgramasController extends Controller {

    public function datatableProgramasSinu($codfacultad) {

        if ($codfacultad == '0') {
            $RelacionProgramasSinuSuneduResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_programas_sinu");
        } else {
            $RelacionProgramasSinuSuneduResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_programas_sinu where codfacultad in ('" . $codfacultad . "') ");
        }



        return Datatables::of($RelacionProgramasSinuSuneduResumen)
                        ->addColumn('action', function ($data) {
                            
                            if (!$data->codprograma_sunedu){
                                 return '<a class="btn btn-xs btn-success" href="/pedidos/programasinu/edit/' . $data->pedido_id . '/' . $data->codfacultad . '/' . $data->codprograma . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                            }else
                            {
                                 return '<a class="btn btn-xs btn-primary" href="/pedidos/programasinu/edit/' . $data->pedido_id . '/' . $data->codfacultad . '/' . $data->codprograma . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                            }
                            
                           
                        })
                        ->make(true);
    }

    public function editProgramaSinu($pedido_id, $codfacultad, $codprograma) {
        $programaSinu = ProgramaSinu::where([
                    ['pedido_id', '=', $pedido_id],
                    ['codfacultad', '=', $codfacultad],
                    ['codprograma', '=', $codprograma],
                ])->first();


        $facultadSinu = FacultadSinu::where([
                    ['pedido_id', '=', $pedido_id],
                    ['codfacultad', '=', $codfacultad],
                    ['codmodalidad', '=', $programaSinu->codmodalidad],
                    ['niv_formacion', '=', $programaSinu->niv_formacion],
                ])->first();


        $facultadSunedu = FacultadSunedu::where([
                    ['codfacultad', '=', $facultadSinu->codfacultad_sunedu]
                ])->first();
        if (!$facultadSunedu) {
            $mensaje = "Por favor primero relacionar Facultad SINU con Facultad SUNEDU";
            $tipoalerta = "alert-warning";
            return view('layouts.mensaje', compact('mensaje', 'tipoalerta'));
        }

        $programaSunedu = DB::connection('pgsql_syscarnes')->select("select codprograma as cod, nomprograma as name from programas_sunedu where codfacultad in ('" . $facultadSinu->codfacultad_sunedu . "') ");

        $formularios = new FormulariosController();




        $selectProgramaSunedu = $formularios->selectSimple($programaSunedu, 'Programa SUNEDU', 'select_programasunedu', $programaSinu->codprograma_sunedu);

        return view('pedidos.edit_programasinu', compact('programaSinu', 'selectProgramaSunedu', 'facultadSunedu'));
    }

    public function updateProgramaSinu(Request $request) {

        $programaSinu = ProgramaSinu::where([
                    ['pedido_id', '=', $request->pedido_id],
                    ['codfacultad', '=', $request->codfacultad],
                    ['codprograma', '=', $request->codprograma],
                ])->first();


        if (!$programaSinu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra una Programa con ese código.'])], 404);
        }

        $programaSunedu = ProgramaSunedu::where([
                    ['codprograma', '=', $request->select_programasunedu],
                ])->first();


        $codProgramaSunedu = $request->select_programasunedu;
        $nomProgramaSunedu = $programaSunedu->nomprograma;

        /* if ($request->method() === 'PATCH') {
          // Creamos una bandera para controlar si se ha modificado algún dato en el método PATCH.
          $bandera = false;
          // Actualización parcial de campos.
          if ($codProgramaSunedu) {
          $programaSinu->select_Programasunedu = $codProgramaSunedu;
          $bandera = true;
          }
          if ($bandera) {
          // Almacenamos en la base de datos el registro.
          $programaSinu->save();
          return response()->json(['status' => 'ok', 'data' => $programaSinu], 200);
          } else {
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 304 Not Modified – [No Modificada] Usado cuando el cacheo de encabezados HTTP está activo
          // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
          return response()->json(['errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato.'])], 304);
          }
          } */

        if (!$codProgramaSunedu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }

        $programaSinu->codprograma_sunedu = $codProgramaSunedu;
        $programaSinu->nomprograma_sunedu = $nomProgramaSunedu;

        $programaSinu->save();
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => $programaSinu], 200);
    }

}
