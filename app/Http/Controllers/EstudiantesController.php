<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EstudiantesController extends Controller {

    public function datatableEstudiantesPedido($codfacultad, $codmodalidad) {

        if ($codfacultad == '0') {
            $EstudiantesPedido = DB::connection('pgsql_syscarnes')->select("select * from vst_estudiantes_pedido where codmodalidad ='" . $codmodalidad . "'  ");
        } else {
            $EstudiantesPedido = DB::connection('pgsql_syscarnes')->select("select * from vst_estudiantes_pedido where codfacultad_sinu in ('" . $codfacultad . "') and codmodalidad ='" . $codmodalidad . "' ");
        }



        return Datatables::of($EstudiantesPedido)
                        ->editColumn('foto', function ($user) {
                            $url = $user->foto;


                            $c = curl_init();
                            curl_setopt($c, CURLOPT_URL, $url);
                            curl_setopt($c, CURLOPT_TIMEOUT, 5);
                            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                            // $content = curl_exec($c);
                            curl_exec($c);
                            //  $status = curl_getinfo($c, CURLINFO_HTTP_CODE);

                            if (!curl_errno($c)) {
                                $info = curl_getinfo($c);
                                $ruta_foto = asset('img/fileupload/noimagenuser.jpg');
                            } else {
                                $info = curl_getinfo($c);
                                $ruta_foto = $url;
                            }
                            $info = curl_getinfo($c);
                            if ($info['http_code'] = 200) {

                                $ruta_foto = $url;
                                /*if (!is_array(@getimagesize($url))) {
                                    $ruta_foto = asset('img/fileupload/noimagenuser.jpg');
                                } else {
                                    $ruta_foto = $url;
                                }*/
                            } else {
                                $ruta_foto = asset('img/fileupload/noimagenuser.jpg');
                            }


                            $info = 'Se tardó ' . $info['total_time'] . ' segundos en enviar una petición a ' . $info['url'] . " con el codigo: " . $info['http_code'] . "  " . "\n";
                            curl_close($c);


                            /* if (!is_array(@getimagesize($url))) {
                              $ruta_foto = asset('img/fileupload/noimagenuser.jpg');
                              } else {
                              $ruta_foto = $url;
                              } */
                            return '<img alt="' . $info . '"  src="' . $ruta_foto . '" width="100"  height="100" >';
                        })
                        ->rawColumns(['foto'])
                        ->make(true);
    }

}
