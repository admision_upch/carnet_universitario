<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\FacultadSinu;
use App\FacultadSunedu;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\componentes\FormulariosController;

class FacultadesController extends Controller {

    public function datatableFacultadesSinuResumen($codfacultad) {
        if ($codfacultad == '0') {
            $FacultadesSinuResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_resumen_facultades_sinu");
        } else {
            $FacultadesSinuResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_resumen_facultades_sinu where codfacultad_sinu in ('" . $codfacultad . "')");
        }

        return Datatables::of($FacultadesSinuResumen)
                        ->addColumn('action', function ($user) {
                            return '<a href="#edit-' . $user->pedido_id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                        })
                        ->make(true);
    }

    public function datatableFacultadesSinu($pedido_id, $codfacultad) {

        if ($codfacultad == '0') {
            //$RelacionFacultadesSinuSuneduResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_facultades_sinu");
            $facultadesSinu = FacultadSinu::where([
                        ['pedido_id', '=', $pedido_id]
                    ])->get();
        } else {
            $facultadesSinu = FacultadSinu::where([
                        ['pedido_id', '=', $pedido_id],
                        ['codfacultad', '=', $codfacultad],
                    ])->get();
            // $RelacionFacultadesSinuSuneduResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_facultades_sinu where codfacultad in ('" . $codfacultad . "') ");
        }



        return Datatables::of($facultadesSinu)
                        ->addColumn('action', function ($data) {
                            return '<a class="btn btn-xs btn-primary" href="/pedidos/facultadsinu/edit/' . $data->pedido_id . '/' . $data->codfacultad . '/' . $data->codmodalidad . '/' . $data->niv_formacion . '" data-target="#ajax_modal" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Editar </a>';
                            //$data->pedido_id
                        })
                        ->make(true);
    }

    public function editFacultadSinu($pedido_id, $codfacultad, $codmodalidad, $niv_formacion) {


        $facultadSinu = FacultadSinu::where([
                    ['pedido_id', '=', $pedido_id],
                    ['codfacultad', '=', $codfacultad],
                    ['codmodalidad', '=', $codmodalidad],
                    ['niv_formacion', '=', $niv_formacion],
                ])->first();
        
        $facultadSunedu = DB::connection('pgsql_syscarnes')->select("select codfacultad as cod, nomfacultad as name from facultades_sunedu");
        $formularios = new FormulariosController();
        $selectFacultadSunedu = $formularios->selectSimple($facultadSunedu, 'Facultad SUNEDU', 'select_facultadsunedu', $facultadSinu->codfacultad_sunedu);
        return view('pedidos.edit_facultadsinu', compact('facultadSinu', 'selectFacultadSunedu'));
    }

    public function updateFacultadSinu(Request $request) {

        $facultadSinu = FacultadSinu::where([
                    ['pedido_id', '=', $request->pedido_id],
                    ['codfacultad', '=', $request->codfacultad],
                 ['codmodalidad', '=', $request->codmodalidad],
                 ['niv_formacion', '=', $request->niv_formacion],
                ])->first();


        if (!$facultadSinu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra una Facultad con ese código.'])], 404);
        }

        $facultadSunedu = FacultadSunedu::find($request->select_facultadsunedu);

        $codFacultadSunedu = $request->select_facultadsunedu;
        $nomFacultadSunedu = $facultadSunedu->nomfacultad;

        if ($request->method() === 'PATCH') {
            // Creamos una bandera para controlar si se ha modificado algún dato en el método PATCH.
            $bandera = false;
            // Actualización parcial de campos.
            if ($codFacultadSunedu) {
                $facultadSinu->select_facultadsunedu = $codFacultadSunedu;
                $bandera = true;
            }
            if ($bandera) {
                // Almacenamos en la base de datos el registro.
                $facultadSinu->save();
                return response()->json(['status' => 'ok', 'data' => $facultadSinu], 200);
            } else {
                // Se devuelve un array errors con los errores encontrados y cabecera HTTP 304 Not Modified – [No Modificada] Usado cuando el cacheo de encabezados HTTP está activo
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato.'])], 304);
            }
        }

        if (!$codFacultadSunedu) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }

        $facultadSinu->codfacultad_sunedu = $codFacultadSunedu;
        $facultadSinu->nomfacultad_sunedu = $nomFacultadSunedu;

        $facultadSinu->save();
        return response()->json(['status' => 'success', 'message' => 'Datos guardados exitosamente', 'data' => $facultadSinu], 200);
    }

}
