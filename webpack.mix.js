const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
        .sass('resources/assets/sass/app.scss', 'public/css');

mix.js(['resources/assets/components/smartwizard/js/jquery.smartWizard.js'], 'public/js/wizard.js')

mix.styles([
    'resources/assets/components/smartwizard/css/smart_wizard.css',
    'resources/assets/components/smartwizard/css/smart_wizard_theme_arrows.css'
], 'public/css/wizard.css');


